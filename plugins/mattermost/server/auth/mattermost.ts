/* eslint-disable */
// @ts-nocheck
import passport from "@outlinewiki/koa-passport";
import Router from "koa-router";
import {
  Strategy as OAuth2Strategy
} from "passport-oauth2";
import accountProvisioner from "@server/commands/accountProvisioner";
import env from "@server/env";
import passportMiddleware from "@server/middlewares/passport";
import httpErrors from "http-errors";
import { request } from '../mattermost.ts';

const router = new Router();
const providerName = "mattermost";
const MATTERMOST_AUTHORIZE_URL = env.MATTERMOST_AUTHORIZE_URL
const MATTERMOST_PROFILE_URL = env.MATTERMOST_PROFILE_URL
const MATTERMOST_TOKEN_URL = env.MATTERMOST_TOKEN_URL
const MATTERMOST_CLIENT_ID = env.MATTERMOST_CLIENT_ID;
const MATTERMOST_CLIENT_SECRET = env.MATTERMOST_CLIENT_SECRET;

const scopes = [];

console.log("loaded");
// Define Errors
function MattermostError(
  message: string = "Mattermost API did not return required fields"
) {
  return httpErrors(400, message, {
    id: "mattermost_error"
  });
}

const strategy = new OAuth2Strategy({
    authorizationURL: MATTERMOST_AUTHORIZE_URL,
    tokenURL: MATTERMOST_TOKEN_URL,
    clientID: MATTERMOST_CLIENT_ID,
    clientSecret: MATTERMOST_CLIENT_SECRET,
    callbackURL: `${env.URL}/auth/${providerName}.callback`,
    passReqToCallback: true,
    scope: scopes
  },
  async function (req, accessToken, refreshToken, _, done) {
    try {
      console.log("accessToken");
      const profile = await request(
        MATTERMOST_PROFILE_URL,
        accessToken,
      )
      if (!profile) {
        throw new MattermostError(
          "Unable to load user profile from Mattermost API"
        );
      }

      const result = await accountProvisioner({
        ip: req.ip,
        team: {
          name: 'Incubateur des Territoires',
          domain: 'incubateur.anct.gouv.fr',
          subdomain: 'incubateur'
        },
        user: {
          name: `${profile.first_name} ${profile.last_name}`,
          email: profile.email
        },
        authenticationProvider: {
          name: providerName,
          providerId: "mattermost"
        },
        authentication: {
          providerId: profile.id,
          accessToken,
          refreshToken,
          scopes
        },
      });
      return done(null, result.user, result);
    } catch (err) {
      return done(err, null);
    }
  });

passport.use(strategy);

router.get(providerName, passport.authenticate(providerName));
router.get(`${providerName}.callback`, passportMiddleware(providerName));

export const name = env.MATTERMOST_DISPLAY_NAME ?? providerName;

export default router;
