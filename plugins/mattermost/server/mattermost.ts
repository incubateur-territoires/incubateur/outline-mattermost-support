/* eslint-disable */
import fetch from "fetch-with-proxy";


export async function request(endpoint: string, accessToken: string) {
  console.log("request");
  const response = await fetch(endpoint, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${accessToken}`,
      "Content-Type": "application/json",
    },
  });
  return response.json();
}
